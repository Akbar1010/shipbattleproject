package org.example;

import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("All set. Get ready to rumble");
        Scanner sc = new Scanner(System.in);
        String[][] array = new String[6][6];
        for (int i = 0; i < array.length; i++) {
            array[0][i] = String.valueOf(i);
            array[i][0] = String.valueOf(i);
        }
        Random rand = new Random();
        int row = rand.nextInt(5) + 1;
        int col = rand.nextInt(5) + 1;
        array[row][col] = "x";
        while (true) {
            try {
                int fireInput = sc.nextInt();
                rangeCheck(fireInput);
                int barInput = sc.nextInt();
                rangeCheck(barInput);
                if (array[fireInput][barInput] == null) {
                    array[fireInput][barInput] = "*";
                }
                if (array[fireInput][barInput].equals("x")) {
                    System.out.println("You have won!");
                    break;
                } else {
                    showTheField(array);
                }
            } catch (InputMismatchException e) {
                System.out.println("invalid input, please enter a number");
                sc.next();
            } catch (OutOfRangeException e) {
                System.out.println(e.message);
            }
        }
    }

    public static void rangeCheck(int input) {
        if (input < 0 || input > 5) {
            throw new OutOfRangeException("out of range, please enter a valid number");
        }
    }

    public static void showTheField(String[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                if (array[i][j] != null && array[i][j].equals("*")) {
                    System.out.print("* |");
                } else if (array[i][j] == null || array[i][j].equals("x")) {
                    System.out.print("- |");
                } else {
                    System.out.print(array[i][j] + " |");
                }
            }
            System.out.println();
        }
    }
}