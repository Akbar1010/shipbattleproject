package org.example;

public class OutOfRangeException extends RuntimeException{
    String message;
    public OutOfRangeException(String message){
        this.message = message;
    }
}
